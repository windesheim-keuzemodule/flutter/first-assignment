import 'package:flutter/material.dart';
import './text_output.dart';

class TextControl extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TextControlState();
  }
}

class _TextControlState extends State<TextControl> {
  String _maintext = 'This text will change';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RaisedButton(
            onPressed: () {
              setState(() {
                _maintext = 'Text has changed!';
              });
            },
            child: Text('changeText'),
            ),
          TextOutput(_maintext) 
      ],
      
    );
  }
}
